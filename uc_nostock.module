<?php
/**
 *  Implements hook_form_alter()
 */
function uc_nostock_form_alter(&$form, &$form_state, $form_id) {
  //Alter add-to-cart form if no stock is enforced for at least one model at time of initial form generation
  if (strstr($form_id, 'uc_product_add_to_cart_form_') !== FALSE) {    
    $node = &$form['node']['#value'];    
    //Add a form element for the stock
    $form['stock'] = array('#type' => 'hidden');
    if (isset($form_state['storage']['uc_skua_combo'])) {
      //Add div wrappers to the submit, qty, and stock form elements
      //Add the nid as a suffix to ensure uniqueness
      $form['actions']['submit']['#prefix'] = '<div class="add_to_cart_replace">';
      $form['actions']['submit']['#suffix'] = '</div>';
      $form['qty']['#prefix'] = '<div class="qty_replace">';
      $form['qty']['#suffix'] = '</div>';
      $form['stock']['#prefix'] = '<div class="stock_replace">';
      $form['stock']['#suffix'] = '</div>';
    }

    $sku = isset($form_state['storage']['uc_skua_sku']) ? $form_state['storage']['uc_skua_sku'] : $node->model;
    //Make stock-related form changes, if any
    if ($sku !== FALSE) {
      //Refresh the stock information if necessary
      if (isset($form_state['temporary']['uc_skua_callback'])) {
        uc_nostock_refresh_stock($node);
      }

      if (uc_nostock_enforce($node, $sku)) {
        if ($node->stock[$sku]->stock <= 0) {
          //Model is out of stock; disable button and qty element
          $form['actions']['submit']['#value'] = t('Out of stock');
          $form['actions']['submit']['#disabled'] = $form['qty']['#disabled'] = TRUE;
        }
        else {
          //Display the available stock
          $form['stock']['#type'] = 'item';
          $form['stock']['#markup'] = 'Stock: ' . $node->stock[$sku]->stock;
        }
      }
    }    
  }

  //Alter buy-it-now form
  if (strstr($form_id, 'uc_catalog_buy_it_now_form_') !== FALSE) {    
    $node = node_load($form['nid']['#value']);
    //If the product has attributes, do nothing
    if (!isset($node->attributes) && uc_nostock_enforce($node, $node->model)
      && $node->stock[$node->model]->stock <= 0) {
      //Model is out of stock; disable button
      $form['actions']['submit']['#value'] = t('Out of stock');
      $form['actions']['submit']['#disabled'] = TRUE;
    }
  }

  //Add custom validation handler to cart page and checkout page forms
  if (in_array($form_id, array('uc_cart_checkout_form', 'uc_cart_checkout_review_form', 'uc_cart_view_form'))) {
    $form['#validate'][] = 'uc_nostock_validate_cc';
  }
}

/**
 * Custom validation handler for cart page and checkout page forms
 */
function uc_nostock_validate_cc($form, &$form_state) {
  if ($form['#form_id'] == 'uc_cart_view_form') {    
    //Only validate if customer wants to checkout
    if ($form_state['clicked_button']['#value'] == $form['actions']['checkout']['checkout']['#value']) {
      //Update the cart contents with any changes the user made
      $contents = uc_cart_get_contents();
      $cart = array();
      foreach ($contents as $index => $product) {
        $qty = uc_nostock_find_product($product->cart_item_id, $form_state['values']['items']);
        if ($qty !== FALSE) {
          //Do not alter the cart
          $cart[$index] = clone $product;
          $cart[$index]->qty = $qty;
        }
      }
    }
  }
  else {
    $cart = uc_cart_get_contents();
  } 

  if (!empty($cart)) {
    $out_of_stock = uc_nostock_validate_contents($cart);
    if (!empty($out_of_stock)) {
      form_set_error('', theme('uc_nostock_order', array('out_of_stock' => $out_of_stock)));
    }
  }
}

/**
 * Finds a product in a set of form values by cart_item_id
 */
function uc_nostock_find_product($cart_item_id, $form_values) {
  foreach ($form_values as $product) {
    if ($product['cart_item_id'] == $cart_item_id) {
      return $product['qty'];
    }
  }

  return FALSE;
}

/**
 * Identifies product(s) in a group for which there is insufficient stock
 */
function uc_nostock_validate_contents($products) {
  //Prevent out of stock models with no stock enforced from being checked out
  $models = array();
  foreach($products as $index => $product) {
    if (uc_nostock_enforce($product, $product->model)) {
      //There may be multiple distinct entries in a cart for the same model
      if (!isset($models[$product->model])) {
        $models[$product->model] = array(
          'qty' => 0,
          'index' => $index,
        );
      }

      $models[$product->model]['qty'] += $product->qty;
    }
  }

  $out_of_stock = array();
  foreach($models as $sku => $info) {
    if ($info['qty'] > $products[$info['index']]->stock[$sku]->stock) {
      $out_of_stock[] = array(
        'product' => $products[$info['index']],
        'qty' => $info['qty'],
      );
    }
  }

  return $out_of_stock;
}

/**
 * Implements hook_uc_skua_callback_alter()
 */
function uc_nostock_uc_skua_callback_alter($form, $form_state) {
  $node = &$form['node']['#value'];
  $parent = '#uc-product-add-to-cart-form-' . $node->nid;
  $commands = array();
  $commands[] = ajax_command_replace($parent . ' .add_to_cart_replace', render($form['actions']['submit']));
  $commands[] = ajax_command_replace($parent . ' .qty_replace', render($form['qty']));
  $commands[] = ajax_command_replace($parent . ' .stock_replace', render($form['stock']));
  return $commands;
}

/**
 * Attaches stock data to a node
 */
function uc_nostock_refresh_stock(&$node) {
  $node->stock = array();
  $stocks = uc_nostock_get_stock(array($node->nid), array($node->model));
  if (!empty($stocks)) {
    foreach ($stocks as $stock) {
      $node->stock[$stock->sku] = $stock;
    }
  }
}

/**
 * Evaluates whether no stock enforcement is enabled for a given sku
 */
function uc_nostock_enforce(&$node, $sku) {
  return (isset($node->stock[$sku]) && $node->stock[$sku]->active == 1 &&
    $node->stock[$sku]->enforce == 1) ? TRUE : FALSE;
}

/**
 * Returns records from table {uc_product_stock} for one or more product models
 */
function uc_nostock_get_stock($nids, $models) {
  //Get the active product adjustments (inactive adjustments may be stored in {uc_product_stock})
  $query = db_select('uc_product_adjustments', 'a');
  $query->join('uc_product_stock', 's', 'a.model = s.sku');
  $query->fields('s');
  $query->condition('s.nid', $nids, 'IN');
  //Get the base sku (not included in {uc_product_adjustments})
  $query2 = db_select('uc_product_stock', 's');
  $query2->fields('s');
  $query2->condition('s.nid', $nids, 'IN');
  $query2->condition('s.sku', $models, 'IN');
  //Get one set of results using the union method
  $query->union($query2, 'UNION ALL');
  return $query->execute()->fetchAll();
}

/**
 * Implements hook_node_load()
 */
function uc_nostock_node_load($nodes, $types) {
  //Filter nodes for products
  $nids = array();
  $models = array();
  foreach($nodes as $node) {
    if (uc_product_is_product($node->type)) {
      $models[] = $node->model;
      $nids[] = $node->nid;
    }
  }

  //Add stock information to nodes
  if (!empty($nids)) {
    $stocks = uc_nostock_get_stock($nids, $models);
    //Add stock information to nodes
    if (!empty($stocks)) {
      foreach ($stocks as $stock) {
        if (!isset($nodes[$stock->nid]->stock)) {
          $nodes[$stock->nid]->stock = array();
        }

        $nodes[$stock->nid]->stock[$stock->sku] = $stock;
      }
    }
  }
}

/**
 * Implements hook_uc_add_to_cart()
 */
function uc_nostock_uc_add_to_cart($nid, $qty, $data) {
  //Load the product model
  $product = uc_product_load_variant($nid, $data);
  //If stock tracking is enabled and no stock is enforced, validate the quantity
  if (uc_nostock_enforce($product, $product->model)) {
    $total = $qty;
    $cart = uc_cart_get_contents();
    foreach ($cart as $item) {
      if ($item->model == $product->model) {
        $total += $item->qty;
      }
    }    

    //Notify the user if they've added more to their cart than is in stock
    if ($total > $product->stock[$product->model]->stock) {
      drupal_set_message(theme('uc_nostock_add_to_cart', array('product' => $product, 'total' => $total,
        'add_to_cart' => $qty)), 'warning');
    }
  }
}

/**
 * Implements hook_uc_order()
 */
function uc_nostock_uc_order($op, $order, $arg2) {
  switch ($op) {
    case 'submit':
      //Prevent out of stock models with no stock enforced from being checked out
      $out_of_stock = uc_nostock_validate_contents($order->products);
      if (!empty($out_of_stock)) {
        return array(
          array(
            'pass' => FALSE,
            'message' => theme('uc_nostock_order', array('out_of_stock' => $out_of_stock)),
          ),
        );
      }

      break;
  }
}

function theme_uc_nostock_order($variables) {
  $out_of_stock = array();
  foreach ($variables['out_of_stock'] as $info) {
    $out_of_stock[] = t('%title (SKU: @sku) -> @qty in stock', array('%title' => $info['product']->title,
      '@sku' => $info['product']->model, '@qty' => $info['product']->stock[$info['product']->model]->stock));
  }

  return t('There is currently insufficient stock to fill the following item(s).  Please alter your order accordingly
    before checking out.') . theme('item_list', array('items' => $out_of_stock));
}

/**
 * Implements hook_theme()
 */
function uc_nostock_theme() {
  return array(
    'uc_nostock_cart_product' => array(
      'variables' => array(
        'in_stock' => 0,
        'cart_qty' => 0
      ),
    ),
    'uc_nostock_add_to_cart' => array(
      'variables' => array(
        'product' => NULL,
        'total' => 0,
        'add_to_cart' => 0,
      ),
    ),
    'uc_nostock_order' => array(
      'variables' => array(
        'out_of_stock' => 0,
      ),
    ),
  );
}

/**
 * Implements hook_uc_product_description()
 */
function uc_nostock_uc_product_description($product) {
  if (uc_nostock_enforce($product, $product->model) && $product->qty > $product->stock[$product->model]->stock) {
    return array(
      array(
        '#theme' => 'uc_nostock_cart_product',
        '#in_stock' => $product->stock[$product->model]->stock,
        '#cart_qty' => $product->qty,
      ),
    );
  }
}

/**
 * Outputs the in-stock qty. in cart view for products with insufficient stock
 */
function theme_uc_nostock_cart_product($variables) {
  $output = array(
    t('In-stock qty.: ') . $variables['in_stock'],    
  );
  
  return theme('item_list', array('items' => $output));
}

/**
 * Displays a warning when a client adds a product/product qty. that is not in stock
 */
function theme_uc_nostock_add_to_cart($variables) {
  return t('You have added more of %product to your cart than we currently have in stock.  Please reduce this
    quantity before checking out.', array('%product' => $variables['product']->title));
}

/**
 * Implements hook_theme_registry_alter()
 */
function uc_nostock_theme_registry_alter(&$theme_registry) {
  //Override default stock edit form theme function
  $theme_registry['uc_stock_edit_form']['function'] = '_uc_nostock_uc_stock_edit_form';
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function uc_nostock_form_uc_stock_edit_form_alter(&$form, &$form_state) {
  //Add an "Enforce no stock" checkbox to each product model
  $node = &$form_state['build_info']['args'][0];
  foreach (element_children($form['stock']) as $id => $value) {
    $sku = $form['stock'][$id]['sku']['#value'];
    $form['stock'][$id]['enforce'] = array(
      '#type' => 'checkbox',
      '#default_value' => (isset($node->stock[$sku]) && (int)$node->stock[$sku]->enforce === 0) ? 0 : 1,
    );
  }

  $form['#submit'][] = '_uc_nostock_edit_submit';
}

/**
 * Custom submit handler for stock edit form
 */
function _uc_nostock_edit_submit($form, &$form_state) {
  //The base submit handler has already inserted any new models in {uc_product_stock}; only updates are required here.
  foreach (element_children($form_state['values']['stock']) as $id) {
    $stock = $form_state['values']['stock'][$id];
    db_update('uc_product_stock')
      ->fields(array('enforce' => $stock['enforce']))
      ->condition('nid', $form['nid']['#value'], '=')
      ->condition('sku', $stock['sku'], '=')
      ->execute();
  }
}

/**
 * Theme override for theme_uc_stock_edit_form() 
 */
function _uc_nostock_uc_stock_edit_form($variables) {
  $form = $variables['form'];
  drupal_add_js('misc/tableselect.js');
  $header = array(
    array('data' => '&nbsp;&nbsp;' . t('Active'), 'class' => array('select-all')),
    array('data' => t('SKU')),
    array('data' => t('Stock')),
    array('data' => t('Threshold')),
    array('data' => t('Enforce no stock')),
  );
  $rows = array();
  foreach (element_children($form['stock']) as $id) {
    $rows[] = array(
      array('data' => drupal_render($form['stock'][$id]['active'])),
      array('data' => drupal_render($form['stock'][$id]['display_sku'])),
      array('data' => drupal_render($form['stock'][$id]['stock'])),
      array('data' => drupal_render($form['stock'][$id]['threshold'])),
      array('data' => drupal_render($form['stock'][$id]['enforce'])),
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows)) . drupal_render_children($form);;
}
