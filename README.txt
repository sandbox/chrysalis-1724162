Copyright Morgan Esch
morgan.esch@gmail.com

No Stock is designed to prevent clients from checking out SKU's that are out of stock.  It does this as follows:

1.  Disables the 'Add to Cart' button.
2.  Displays model stock on add-to-cart forms.
3.  Disables the 'Buy it now' button.
4.  Notifies the user when they add an SKU to the cart.
5.  Sets a form error on the cart view, checkout, and checkout review forms.
6.  Prevents an order from being submitted.

This behaviour is enabled by default when the module is installed.  Fine-grained per-sku toggling is provided via
the product stock edit page.  To work, stock tracking AND no stock behaviour must be enabled.

If you have uc_attribute module enabled, No Stock integrates with the uc_skua (SKU Ajax Adjustment) module to
update the following form elements when the client changes the SKU via the UI:
1.  Stock quantity.
2.  Add-to-cart button.
3.  Quantity field.

Form error and other messages are themeable, and therefore can be overriden/customized in your template.php theme file.
